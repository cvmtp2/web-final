JEUX - CAROM 

		**********
		* �quipe *
		**********

- Xi Duo Zhang
- Vinh Vincent
- Ferrahi Lyes
- Sartore-Devasse Hoanui
- Vargas Ocando Paola Andreina

		*************** 
		* Utilisation *
		***************

1 - Allez dans le dossier [localhost]\web-final\Serveur (chemin host: http://149.28.36.141/public_html/carom/Web/login.php)
2 - Lancer une commande powershell depuis le dossier
3 - Saisir la commande : npm i sqlite3
4 - Saisir la commande : node node.js
5 - Ouvrir Google Chrome
6 - Lancer localhost
7 - Lancer le fichier login.php
8 - Saisir votre nom de joueur ou vous inscrire
9 - Si succes, cela vous menera dans le fichier Lobby.php 
10 - Choisir une partie pour jouer.
11 - Joeur 2 doit suivre les etapes 1 a 10 pour entrer dans la partie avec vous

		************
		* Features *
		************

   	* Lobby de connection *
- Creation des usagers
- Login des usagers

 	* Choix de partie *
- Affichage parties disponibles
- Start une nouvelle partie
- Pairage des joueurs selon la partie choisi

      	* D�but de partie *
- Entrer dans la partie
- Position de base des boules

	* Interaction durant la partie *
- D�placement des boules dans la bonne direction (en fonction des angles) sur les bords de la table.  
- �change des infos de positions balle et queue client/serveur
- Suivi en temps r�el des mouvement de balle et queue de l'adversaire 

	* Gestion des tours de jeu (� qui est le tour) *
- Passer le tour
- Fin de tour

    	* Pointage *
- Faire des points
- Afficher les points gagnes

	* Fin de partie *
- Bouton fin de partie pour la simulation (ou 20 points gagn� met fin � la partie)
- Message d'alerte pour afficher les pointages finaux
- Retour a la page Lobby.php 

	* Sauvegarde des donn�es *
- Toutes les donnees sont sauvegardees dans le serveur SQLite


		**********
		* Bogues *
		**********
- Probleme de synchronisation dans l'interaction durant la partie (derniere minute)
- La queue du joueur auquel n'est pas le tour, n'est pas desactivee (derniere minute)
- Le Lobby ne montre qu'une partie a la fois (Pas completee)
- Probl�me de possibilite de jouer une autre partie (besoin d'un nouveau IDpartie)
- Probl�me d'insertion du joueur gagnant dans la BD 
- Les calculs de mouvements de balles n'est pas aboutis 