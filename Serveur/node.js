/*--------------  CONNECTION A LA BASE DE DONNEÉ ---------------------- */


/******************** CREATION BASE DE DONNÉ SQLITE***************************/
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('mydb.db');
var check;
var count = 0;

//db.run("DROP TABLE joueurs");
db.serialize(function()         
{
  db.run("CREATE TABLE if not exists joueurs (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT NOT NULL , score INTEGER, idpartie INTEGER) ");
  
});


//insererJoueur(db,"lyes");
//afficherJoueur(db);
//insert(db,"new")
//authenticate(db,"new");
//afficherJoueur(db);
//insererJoueur(db,"dav2");
//afficherJoueur(db);
/****************************************************************************/


//Global variables
let positions = [];
positions[0] = 0;
positions[1] = 0;
let playersMaster = [];
let uniqueIdPartie = [];
let tps;
/*
playersMaster["joueur1"] = {};
playersMaster["joueur2"] = {};
*/
let listening_port = 3000;

let express = require('express');

let app = express();
app.get('/*', function(req, res, next) {	
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
})
.get('/api/id/:nom/:idPartie', function(req, res) {
    console.log(req.params.nom); 	
    res.send(sendID(req.params.nom, req.params.idPartie));
    res.end();
})
.get('/api/debut/:idPartie/:id', function(req, res) {
    console.log(req.params.id); 
	console.log(req.params.idPartie); 
    res.send(whoStart(req.params.idPartie,req.params.id));
    res.end();
})  
.get('/api/finTour/:idPartie', function(req, res) {
    changeEtat(req.params.idPartie);   
    res.end();
}) 
.get('/api/boucleTour/:idPartie/:id', function(req, res) {
    res.send(boucleEtat(req.params.idPartie, req.params.id));
    res.end();
})
.get('/api/playerListener/:idPartie/:id', function(req, res) {
    res.send(playerListener(req.params.idPartie, req.params.id));
    res.end();
})
.get('/api/positionCanneSend/:idPartie/:pos', function(req, res) {
    changePos(req.params.idPartie , req.params.pos);
    res.end();
})  
.get('/api/positionCanneGet/:idPartie', function(req, res) {
    //res.send(positions);
	res.send(sendPos(req.params.idPartie));
    res.end();
})  
.get('/api/login/:username', function(req, res) 
{	
    res.send(authenticate(db,req.params.username));
    res.end();
})
.get('/api/singin/:username', function(req, res) {
    //res.send(positions);
    insert(db,req.params.username);
    res.send("inscrit");
    console.log(req.params.username + " - INSERT")
    //afficherJoueur(db);
    res.end();
})  
.get('/api/insert/:username', function(req, res) {
    insert(db, req.params.username);
    console.log(req.params.username + " - INSERT")
	res.send("true");
    res.end();
}) 
.get('/api/winner/:username', function(req, res) {
    insertWin(db, req.params.username);
    console.log(req.params.username + " - INSERT")
	res.send("true");
    res.end();
})
.get('/api/addScore/:idPartie/:id', function(req, res) {
    addScore(req.params.idPartie, req.params.id);
    res.end();
})
.get('/api/showDB', function(req, res) {
    selectAll(db);
    res.end();
})

//Gestion des pages introuvables
app.use(function(req, res, next){
    res.setHeader('Content-Type', 'text/plain');
    res.status(404).send('Page introuvable !'); 
})

app.listen(listening_port);


/*************************** FUNCTIONS ***************************/ 

function sendArray(array){
    let idTemp = [array];    
	console.log(idTemp)
    return idTemp;
}
function addScore(idparty , id){
	if(id == 1){
		playersMaster[idparty]["joueur2"]["points"] += 1; 
	}
	else{
		playersMaster[idparty]["joueur1"]["points"] += 1; 
	}
}
function playerListener(idparty , id){
    if(id == 1){
        return sendArray(playersMaster[idparty]["joueur2"]);
    }
    else{
        return sendArray(playersMaster[idparty]["joueur1"]);
    }
}
function sendID(nom, idparty){    	
	//AVAILABLE OPEN PARTY
	let assocP2 = false;	
	if(uniqueIdPartie.length != 0){
		for(let i = 0; i < uniqueIdPartie.length ; i++ ){
			console.log("ID PARSE : "+uniqueIdPartie[i]);
			if(playersMaster[uniqueIdPartie[i]]["joueur1"]["nom"] !== undefined && playersMaster[uniqueIdPartie[i]]["joueur2"]["nom"] === undefined){
				//ADD Player2
				playersMaster[uniqueIdPartie[i]]["joueur2"]["nom"] = nom;
				console.log("ADDED P2 TO PARTI N: " + uniqueIdPartie[i]);
				assocP2 = !assocP2;
			}
		}
	}
	if(!assocP2){
		uniqueIdPartie.push(idparty);
		let idPartie =  idparty ;
		
		console.log("ARRAY : " + uniqueIdPartie);
		console.log("ID : " + idPartie);
		playersMaster[idPartie] = [];
		playersMaster[idPartie]["joueur1"] = {};
		playersMaster[idPartie]["joueur2"] = {};
		playersMaster[idPartie]["position"] = {};
		playersMaster[idPartie]["position"]["x"] = 0;
		playersMaster[idPartie]["position"]["y"] = 0;
		playersMaster[idPartie]["joueur1"]["id"] = 1;
		playersMaster[idPartie]["joueur2"]["id"] = 2;
		playersMaster[idPartie]["joueur1"]["idPartie"] = idPartie;
		playersMaster[idPartie]["joueur2"]["idPartie"] = idPartie;
		if(playersMaster[idPartie]["joueur1"]["nom"] === undefined ){
			playersMaster[idPartie]["joueur1"]["nom"] = nom;
		}
	}	
	for(let i = 0; i < uniqueIdPartie.length ; i++ ){
		console.log("ID PARSE : "+uniqueIdPartie[i]);
		if(playersMaster[uniqueIdPartie[i]]["joueur1"]["nom"] == nom){
			console.log(playersMaster[uniqueIdPartie[i]])
			console.log(" - ")
			console.log(playersMaster)
			return sendArray(playersMaster[uniqueIdPartie[i]]["joueur1"]);
		}
		else if(playersMaster[uniqueIdPartie[i]]["joueur2"]["nom"] == nom){
			console.log(playersMaster[uniqueIdPartie[i]])
			console.log(" - ")
			console.log(playersMaster)
			return sendArray(playersMaster[uniqueIdPartie[i]]["joueur2"]);
		}
	}
}
function randNum(min, max){
	let temp = Math.floor(Math.random() * max) + min ;
	console.log("rand : " + temp)
	uniqueIdPartie.push(temp);	
	return temp;
}

function whoStart(idparty,id){
	
    let whoStart = Math.round(Math.random());
    if(playersMaster[idparty]["joueur1"]["start"] === undefined ){
        if(whoStart == 0){
            playersMaster[idparty]["joueur1"]["start"] = true;
            playersMaster[idparty]["joueur1"]["etat"] = true;
            playersMaster[idparty]["joueur2"]["start"] = false;
            playersMaster[idparty]["joueur2"]["etat"] = false;
        }
        else{
            playersMaster[idparty]["joueur1"]["start"] = false;
            playersMaster[idparty]["joueur1"]["etat"] = false;
            playersMaster[idparty]["joueur2"]["start"] = true;
            playersMaster[idparty]["joueur2"]["etat"] = true;
        }
        playersMaster[idparty]["joueur1"]["win"] = false;
        playersMaster[idparty]["joueur2"]["win"] = false;
        playersMaster[idparty]["joueur1"]["points"] = 0;
        playersMaster[idparty]["joueur2"]["points"] = 0;
    }      
    if(playersMaster[idparty]["joueur1"]["id"] == id){
        return sendArray(playersMaster[idparty]["joueur1"]["start"]);        
    } 
    else{
        return sendArray(playersMaster[idparty]["joueur2"]["start"]);
    }  
}
function changeEtat(idparty){
    playersMaster[idparty]["joueur1"]["etat"] = !playersMaster[idparty]["joueur1"]["etat"];
    playersMaster[idparty]["joueur2"]["etat"] = !playersMaster[idparty]["joueur2"]["etat"];
}
function addPoints(id){
    if(playersMaster["joueur1"]["id"] == id){
        playersMaster["joueur1"]["points"] += 1;
        return sendArray(playersMaster["joueur1"]["points"]);        
    } 
    else{
        playersMaster["joueur2"]["points"] += 1;
        return sendArray(playersMaster["joueur2"]["points"]);
    } 
}
function setWinner(id){
    if(playersMaster["joueur1"]["id"] == id){
        playersMaster["joueur1"]["win"] = true;
        return sendArray(playersMaster["joueur1"]["win"]);        
    } 
    else{
        playersMaster["joueur2"]["win"] = true;
        return sendArray(playersMaster["joueur2"]["win"]);
    } 
}
function boucleEtat(idparty , id){   
    if(playersMaster[idparty]["joueur1"]["id"] == id){
        return sendArray(playersMaster[idparty]["joueur1"]["etat"]);        
    } 
    else{
        return sendArray(playersMaster[idparty]["joueur2"]["etat"]);
    }  
}
function changePos (idparty, pos)
{
    //positions [0] = pos.split(';')[0];//X
    //positions [1] = pos.split(';')[1];//Y
	
	playersMaster[idparty]["position"]["x"] = pos.split(';')[0];//X
	playersMaster[idparty]["position"]["y"] = pos.split(';')[1];//Y
	
	return sendArray(playersMaster[idparty]["position"]);
}
function sendPos (idparty)
{
	return sendArray(playersMaster[idparty]["position"]);
}

/*-------------------   DAO    ---------------------- */

//db.close();

function authenticate(db, username)
{
    count = 0;
    
    /*
    let request = "SELECT * FROM joueurs where username = " + '"'+username+'"';	
    db.each(request, function(err, row) 
    {		
		if(row != 0){
			count = 1;
			console.log(row.id + " | " + row.username);  
			return sendArray(true);
		}
		  
			
    });  
    if(count == 0){
        console.log("FIN")
        return sendArray(false);
    }
    */
    let temp = false;
    let protest = new Promise(function(resolve, reject){
        let request = "SELECT * FROM joueurs where username = " + '"'+username+'"';	
        db.each(request, function(err, row) 
        {		
            if(row != 0){
                
                //return sendArray(temp);
            }
            count = 1;
            console.log(row.id + " | " + row.username);  
            temp = true;
            return sendArray(temp);
                
        });  
        resolve(console.log("DONE"));
    })
    protest.then(function(fromResolve){
        return sendArray(temp);
    })
	
	
}

function insert(db, username){
    let request = 'INSERT INTO joueurs (username) VALUES (?)';
    var stmt = db.prepare(request);
    stmt.run(username);
    request = 'select * FROM joueurs ';
    db.each(request, function(err, row) 
    {
        console.log(row.id + " | " + row.username);
        console.log(row)
    });    
}
function selectAll(db){
    let request = 'SELECT * FROM joueurs';
    var stmt = db.prepare(request);
    db.each(request, function(err, row) 
    {
        console.log(row)
    });    
}
function insertWin(db, username, score){
    let request = 'UPDATE joueurs SET score = '+score+' WHERE  username = '+username;
    db.each(request, function(err, row) 
    {
        console.log(row)
    });    
}


  db.run("CREATE TABLE if not exists joueurs (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT NOT NULL , score INTEGER, idpartie INTEGER) ");
  

function insererJoueur(db,joueur)
{    
    var stmt = db.prepare("INSERT INTO joueur1 VALUES (?)");
    stmt.run(joueur);
}



function afficherJoueur(db)
{
    console.log("debut");
    db.each("SELECT rowid AS id, info FROM joueur1", function(err, row) 
    {
        console.log(row.id + ": " + row.info);
    });

    console.log("fin");
}