<?php
	require_once("action/CommonAction.php");

	class LoginAction extends CommonAction {

		public function __construct() {
			parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			if (isset($_GET["username"])) {
				header("location:lobby.php");
				$data = [];
				$data["username"] = $_POST["username"];
				//$data["pwd"] = $_POST["pwd"];
				$this->key = CommonAction::callAPI("login", $data);
				echo $this->key;
				if($this->key == "EMPTY_USERNAME" || $this->key == "USER_NOT_FOUND" || $this->key == "INVALID_USERNAME_PASSWORD")
				{
					$visibility = CommonAction::$VISIBILITY_PUBLIC;
				}
				else
				{
					$visibility = CommonAction::$VISIBILITY_MEMBER;
				}

				if ($visibility > CommonAction::$VISIBILITY_PUBLIC) {
					
					$_SESSION["username"] = $_POST["username"];
					$_SESSION["visibility"] = CommonAction::$VISIBILITY_MEMBER;
					$_SESSION["key"] = $this->key;
					header("location:lobby.php");
					 
					exit;
				}
				else {
						$this->wrongLogin = true;
				}
			}
		}
	}