<?php	
	require_once("action/GameAction.php");	
	$action = new GameAction();
	$action->execute();
	require_once("partial/header.php");	
?>
<script src="js/js-game.js"></script>
<script>
idJoueur = <?php echo $_SESSION['session_idJoueur']; ?>;
idPartie = <?php echo $_SESSION['session_idPartie']; ?>;
</script>
<div id="gameContainer">
	<div class="queue" id="queueJ1"></div>
	<div class="queue" id="queueJ2"></div>	
	<div id="table">
		<div class="balle" id="balleRouge"></div>
		<div class="balle" id="balleJaune"></div>
		<div class="balle" id="balleBlanche"></div>
	</div>
	<div id="gameFooter">
		<div class="gauche">
			<div class="tour">
				<div class="tourIcon" id="tonTour"></div>
				<h3 id="nomJ1">Joueur 1</h3>
			</div>
			<div class="clear"></div>
			<h4 id="pointsJ1">0 Points</h4>
		</div>	
		<button id="passerTour">Passer mon tour</button>
		<button id="finPartie" style="color: red;">FIN</button>
		
		<div class="droite">
			<div class="tour">
				<div class="tourIcon" id="pasTonTour"></div>
				<h3 id="nomJ2">Joueur 2</h3>
			</div>
			<h4 id="pointsJ2">0 Points</h4>
		</div>
	</div>
</div>
<?php
	require_once("partial/footer.php");