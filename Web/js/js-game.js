//Variables Animation helper
let spriteListBoule = [];
let spriteList = [];

//Variables Queue Helper
let mousePosX= 0;
let mousePosY = 0;
//mousePosXAutre et Y est x,y de la position sur le page au complet
let mousePosXAutre = 0;
let mousePosYAutre = 0;
let charge = 0;
let mouseQueueStartPosX = 0;
let mouseQueueStartPosY = 0;

let tour = false;
//Variables Partie Helper
let idJoueur = null;
let nomJ1;
let nomJ2;
let scoreJ1;
let scoreJ2;
let nowPlaying = null;
let listeInfo = [];
let requete = null;
let id = null;
let listening_port = 3000;
//let listening_IP = "149.28.36.141";
let listening_IP = "localhost";
let j1 = 0;
let j2 = 1;   
let idPartie;

window.onload = function () {  
    
    // A effacer apres test
    //debutPartie()

    //spritesPartie();
    debutPartie();
    
    
    /* document.getElementById("debut").onclick = function()
    {   
        debutPartie();
        
    } */
	document.getElementById("finPartie").onclick = function()
    {   
		if(scoreJ1 > scoreJ2){
			$.get('http://' + listening_IP + ':'+listening_port+'/api/winner/'+nomJ2, {}, function(data){});
			alert(nomJ2 + " a gagné")
			window.location.href = "lobby.php";	
		}
		else if(scoreJ1 < scoreJ2){
			$.get('http://' + listening_IP + ':'+listening_port+'/api/winner/'+nomJ1, {}, function(data){});
			alert(nomJ1 + " a gagné")
			window.location.href = "lobby.php";	
		}
		else if(scoreJ1 == scoreJ2){
			$.get('http://' + listening_IP + ':'+listening_port+'/api/winner/'+nomJ1, {}, function(data){});
			$.get('http://' + listening_IP + ':'+listening_port+'/api/winner/'+nomJ2, {}, function(data){});
			window.location.href = "lobby.php";	
		}
        
    }
    if(!(window.location.href.includes("game.php"))){
        document.getElementById("finTour").onclick = function()
        {
            finTour();
            boucleTour();
        }
    }else{
		document.getElementById("passerTour").onclick = function()
		{
            tour = false;
			finTour();
			boucleTour();
		}
    }
    //////* Animation Start *//////
    mainTick();
    playerListener();
}
function getNowPlaying(){
    let nowPlaying = null;

    // console.log("tour"+ tour + "  idJoueur"+idJoueur);
    if(tour == true){
        nowPlaying = idJoueur;
    }else{
        if(idJoueur == 1){
            nowPlaying = 2;
        } else{
            nowPlaying = 1;
        }
    }
    // console.log("nowPlaying" + nowPlaying);

    return nowPlaying;
}
function showTour(nowPlaying){
    // console.log("called")
    if (nowPlaying == 1){
        document.getElementById("tourJ1").className = "tourIcon , tonTour";
        document.getElementById("queueJ1").className = "queue";
        document.getElementById("tourJ2").className = "tourIcon , pasTonTour";
        document.getElementById("queueJ2").className = "queue , queueJ2Disabled";
    }else if(nowPlaying == 2){
        document.getElementById("tourJ2").className = "tourIcon , tonTour";
        document.getElementById("queueJ2").className = "queue";
        document.getElementById("tourJ1").className = "tourIcon , pasTonTour";
        document.getElementById("queueJ1").className = "queue , queueJ2Disabled";
    }
}

//////////***********Animation manager**************/////////
function mainTick()
{

	//Animation
	for (let i = 0; i < spriteListBoule.length; i++) {
		const element = spriteListBoule[i];
        element.tick();	
    } 
    
    for (let i = 0; i < spriteList.length; i++) {
        const element = spriteList[i];
          element.tick();
    }

    showTour(getNowPlaying());

	//Rappel (60fps)
	window.requestAnimationFrame(mainTick);//delai intelligent pour les animations, remplace settimeout
}
function randNum(min, max){
	let temp = Math.floor(Math.random() * max) + min ;
	// console.log("rand : " + temp)
	return temp;
}
function spritesPartie(){
    //spriteList.push(new Queue("1"));
    //spriteList.push(new Queue("J2"));
    spriteListBoule.push(new Boule("Jaune"));
    spriteListBoule.push(new Boule("Blanche"));
    //spriteListBoule.push(new Boule("Rouge"));
    

}


//////////***********Partie manager**************/////////
function playerListener(){
    $.get('http://' + listening_IP + ':'+listening_port+'/api/playerListener/'+idPartie+'/0', {}, function(data){
		nomJ1= data[0].nom;
			$("#nomJ1").html(data[0].nom);
    });   
    $.get('http://' + listening_IP + ':'+listening_port+'/api/playerListener/'+idPartie+'/1', {}, function(data){
		nomJ2= data[0].nom;
			$("#nomJ2").html(data[0].nom);
    });     
}

function debutPartie()
{
    $.get('http://' + listening_IP + ':'+listening_port+'/api/debut/'+idPartie+'/'+idJoueur, {}, function(data){
        // console.log(data[0])
        if(data[0])
        {
            console.log("c'est toi qui commence")
            tour = true;
            spriteList.push(new Queue(idJoueur));
            envoiInclinaisonQueu();
        }
        else
        {
            console.log("c'est l'autre joueur qui commence")
            if(idJoueur == 1)
            {
                spriteList.push(new Queue("2"));
            }
            else
            {
                spriteList.push(new Queue("1"));
            }
            tour=false;
            boucleTour();
        }
    });
    spritesPartie();
}
function finTour()
{
    $.get('http://' + listening_IP + ':'+listening_port+'/api/finTour/'+idPartie, {}, function(){
        console.log("Ton tour est fini, c'est a l'autre!")
        tour = false;
        spriteList.pop();

        if(idJoueur == 1)
        {
            //$("#queueJ2").show();
            spriteList.push(new Queue("2"));
            //$("#queueJ1").hide();
        }
        else
        {
            //$("#queueJ1").show();
            spriteList.push(new Queue("1"));
            //$("#queueJ2").hide();
        }
        clearInterval(requete);
    });
}
function boucleTour()
{
    id = setInterval(function(){
        if(tour == false)
        {
            recoitInclinaisonQueu();
            $.get('http://' + listening_IP + ':'+listening_port+'/api/boucleTour/'+idPartie+'/'+idJoueur, {}, function(data){
                if(data[0])
                {
                    console.log("c'est ton tour!")
                    spriteList.pop();
                    spriteList.push(new Queue(idJoueur));
                    tour = true;
                    clearInterval(id);
                    envoiInclinaisonQueu();
                }
            });			
        }  
		$.get('http://' + listening_IP + ':'+listening_port+'/api/playerListener/'+idPartie+'/0', {}, function(data){
            scoreJ1 = data[0].points;
            if(scoreJ1 == 20)
            {
                if(idJoueur == 2)
                {
					$.get('http://' + listening_IP + ':'+listening_port+'/api/winner/'+nomJ2, {}, function(data){});
                    alert("you win");
                }
                else
                {
					$.get('http://' + listening_IP + ':'+listening_port+'/api/winner/'+nomJ1, {}, function(data){});
                    alert("you lose");
                }
                window.location.href = "lobby.php";	
            }
               $("#pointsJ2").html(data[0].points + " Points");
        });
		$.get('http://' + listening_IP + ':'+listening_port+'/api/playerListener/'+idPartie+'/1', {}, function(data){
            scoreJ2 = data[0].points;
            if(scoreJ2 == 20)
            {
                if(idJoueur == 1)
                {
					$.get('http://' + listening_IP + ':'+listening_port+'/api/winner/'+nomJ1, {}, function(data){});
                    alert("you win");
                }
                else
                {
					$.get('http://' + listening_IP + ':'+listening_port+'/api/winner/'+nomJ2, {}, function(data){});
                    alert("you lose");
                }
                window.location.href = "lobby.php";	
            }
               $("#pointsJ1").html(data[0].points + " Points");
        });


    
}, 50);
    
}

//////////***********Queue manager**************/////////
    //Inclinaison Queue
function envoiInclinaisonQueu()
{
    requete = setInterval(function(){
            $.get('http://' + listening_IP + ':'+listening_port+'/api/positionCanneSend/'+idPartie+'/'+mousePosX+';'+mousePosY, {}, function(){
                
            }); 
            $.get('http://' + listening_IP + ':'+listening_port+'/api/playerListener/'+idPartie+'/0', {}, function(data){
                scoreJ1 = data[0].points;
                if(scoreJ1 == 20)
                {
                    if(idJoueur == 2)
                    {
                        alert("you win");
                    }
                    else
                    {
                        alert("you lose");
                    }
                    window.location.href = "lobby.php";	
                }
                   $("#pointsJ2").html(data[0].points + " Points");
            });
            $.get('http://' + listening_IP + ':'+listening_port+'/api/playerListener/'+idPartie+'/1', {}, function(data){
                scoreJ2 = data[0].points;
                if(scoreJ2 == 20)
                {
                    if(idJoueur == 1)
                    {
                        alert("you win");
                    }
                    else
                    {
                        alert("you lose");
                    }
                    window.location.href = "lobby.php";	
                }
                   $("#pointsJ1").html(data[0].points + " Points");
            });    
    
        }, 50);
    
}
function recoitInclinaisonQueu()
{
    $.get('http://' + listening_IP + ':'+listening_port+'/api/positionCanneGet/'+idPartie, {}, function(data){
        //console.log(data[0] + "px" );
        mousePosXAutre = data[0].x;
        mousePosYAutre = data[0].y;
		//console.log(mousePosXAutre)
		//console.log(mousePosYAutre)
        //document.getElementById("test").style.left = data[0] + "px";  
        //document.getElementById("test").style.top = data[1] + "px";
    });
}
// Charge Queue
/*
function envoiChargeQueu()
{
    requete = setInterval(function(){
            $.get('http://' + listening_IP + ':'+listening_port+'/api/positionCanneSend/'+mouseQueueStartPosX+';'+mouseQueueStartPosY+';'+charge, {}, function(){
                
            });     
    
        }, 50);
    
}
function recoitChargeQueu()
{
    $.get('http://' + listening_IP + ':'+listening_port+'/api/positionCanneGet/', {}, function(data){
        console.log(data[0] + "px" );
        mouseQueueStartPosX = data[0];
        mouseQueueStartPosY = data[1];
        charge = data[2];
    });
}
*/
//////////***********Balle manager**************/////////
function requeteForceBalle()
{
    $.get('http://' + listening_IP + ':'+listening_port+'/api/requeteForceBalle/'+idJoueur, {}, function(data){
        
    });
}
function recoitForceBalle()
{
    $.get('http://' + listening_IP + ':'+listening_port+'/api/recoitForceBalle/'+idJoueur, {}, function(data){
        
    });
}

////////////***********Joueurs manager**************/////////
function getIdJoueur(nom)
{
    $.get('http://' + listening_IP + ':'+listening_port+'/api/id/'+nom+'/'+randNum(1, 100), {}, function(data){
        idJoueur = data[0].id;
		idPartie = data[0].idPartie;
        console.log("ID : "+idJoueur);
		console.log("ID PARTIE : "+idPartie);
    });
}
function initPlayers(nom)
{
    $.get('http://' + listening_IP + ':'+listening_port+'/api/init/'+nom, {}, function(data){
    console.log(data[j1].nom);
    });
    //idJoueur = data["0"]["id"]
}
function requeteInfoJoueur()
{
    $.get('http://' + listening_IP + ':'+listening_port+'/api/infoJoueur/'+idJoueur, {}, function(data){
        
    });
}
function requeteJoueurFinPartie()
{
    $.get('http://' + listening_IP + ':'+listening_port+'/api/joueurFinpartie/'+nom, {}, function(data){
        idJoueur = data[0];
        console.log(idJoueur);
    });
}
function requetePartieFinPartie()
{
    $.get('http://' + listening_IP + ':'+listening_port+'/api/id/PartieFinpartie'+nom, {}, function(data){
        idJoueur = data[0];
        console.log(idJoueur);
    });
    
}