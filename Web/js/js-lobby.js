//Variables Partie Helper
let idJoueur = null;
let nomJoueur = null;
let listeInfo = [];
let requete = null;
let id = null;
let listening_port = 3000;
//let listening_IP = "149.28.36.141";
let listening_IP = "localhost";
let j1 = 0;
let j2 = 1;   
let idPartie;

window.onload = ()=>{

	let buttonParties = document.getElementById("buttonParties");
	let cadreParties = document.getElementById("lobbyAffichageParties");
	let buttonScores = document.getElementById("buttonScores");
	let cadreScores = document.getElementById("lobbyAffichageScores");
	let buttonInfos = document.getElementById("buttonInfos");
	let cadreInfos = document.getElementById("lobbyAffichageInfos");
	buttonInfos.innerHTML = nomJoueur || "Joueur";
	let lobby = document.getElementById("lobbyContainer");
	$("#msg").html(nomJoueur+", selectionne une des parties disponibles pour jouer")
	buttonParties.onclick = function()
    {
		cadreParties.style.visibility = "visible";
		cadreInfos.style.visibility = "hidden";
		cadreScores.style.visibility = "hidden";
	}
	buttonScores.onclick = function()
    {
		console.log("scores")
		cadreScores.style.visibility = "visible";
		cadreInfos.style.visibility = "hidden";
		cadreParties.style.visibility = "hidden";
		
	}
	buttonInfos.onclick = function()
    {
		cadreInfos.style.visibility = "visible";
		cadreParties.style.visibility = "hidden";
		cadreScores.style.visibility = "hidden";
	}


	let nouvellePartie = document.getElementById("nouvellePartie").onclick = function()
    {
		lobby.className = "inactive";
		document.getElementById("messageBox").style.visibility = "visible";		
	}
	let curentPartie = document.getElementById("curentPartie").onclick = function()
    {
		window.location.href = "game.php";		
	}
	document.getElementById("back").onclick = function(){
		lobby.className = "";
		document.getElementById("messageBox").style.visibility = "hidden";
	}
	document.getElementById("enter").onclick = function(){
		//0 = carom libre; 1 = une bande; 2 = trois bandes;
		console.log(document.getElementById("typePartie").selectedIndex)
		//appel de nouvelle partie; go to game.php
		window.location.href = "game.php";	
	}
	choixButton();
	playerListener();	
}
function playerListener(){
    id = setInterval(function(){
        $.get('http://' + listening_IP + ':'+listening_port+'/api/playerListener/'+idPartie+'/'+idJoueur, {}, function(data){
			$("#playerName").html(data[0].nom);
		});    
	}, 50);    
}

function getIdJoueur(nom)
{
    $.get('http://' + listening_IP + ':'+listening_port+'/api/id/'+nom+'/'+randNum(1, 100), {}, function(data){
        idJoueur = data[0].id;
		idPartie = data[0].idPartie;
        console.log("ID : "+idJoueur);
		console.log("ID PARTIE : "+idPartie);
		console.log("1")		
    });
}
function randNum(min, max){
	let temp = Math.floor(Math.random() * max) + min ;
	return temp;
}
function choixButton(){
	let choix = document.getElementsByClassName("buttonLobby");
	
	for (let i = 0; i < choix.length; i++) {
		const element = choix[i];
		element.onclick = function(e){
			let value = element.getAttribute("value");
			// Afficher tous les parties disponible
			if(value === "Parties"){
				console.log("entrer Parties");
				window.location.assign("game.php?idj="+idPartie+"?idp="+idPartie);
			}
			//Afficher les infos du joueur
			else if(value === "Scores"){
				console.log("entrer Scores");
			}
		}
	}
}

function affichagePartie(response){
	let partiesContainer = document.getElementById("lobbyAffichageParties")
	partiesContainer.innerHTML = "";
	let template = document.querySelector("#templatePartie").innerHTML;
	
	//titre
	let node = document.createElement("div");
	node.innerHTML = "Selectionne une des parties disponibles pour jouer";
	document.getElementById("lobbyAffichage").appendChild(node);
	//slot new game
	let slotPartie = document.createElement("div");
	slotPartie.setAttribute("id", "nouvellePartie");
	slotPartie.setAttribute("class", "slotPartie");
	let num = document.createElement("h2");
	num.innerHTML = "+";
	num.setAttribute("class", "idPartie");
	num.setAttribute("id", "new");
	let newDiv = document.createElement("div");
	let h4 = document.createElement("h4");
	h4.innerHTML = "Nouvelle partie"
	let h5 = document.createElement("h5");
	h5.innerHTML = "Tu seras le joueur 1"
	newDiv.appendChild(h4);
	newDiv.appendChild(h5);
	slotPartie.appendChild(num);
	slotPartie.appendChild(newDiv);

	for (var i = 0; i < response.length; i++) {
		let node = document.createElement("div");
		node.setAttribute("class","slotPartie");
		node.setAttribute("id",response[i].id); // attribuer le id de la partie pour le id de chaque div de la partie
		node.setAttribute("name", "partie");
		node.innerHTML = template;
		node.querySelector("h2").innerHTML = response[i].name;
		node.querySelector(".variant").innerHTML = response[i].variant;
		node.querySelector(".status").innerHTML = response[i].status;
		
		partiesContainer.appendChild(node);
	}
}

function affichageScore(response){
	document.getElementById("lobbyAffichage").innerHTML = "";
	let template = document.querySelector("#templateScore").innerHTML;

	for (var i = 0; i < response.length; i++) {
		let node = document.createElement("div");
		node.setAttribute("class","score");
		node.setAttribute("name", "score");
		node.innerHTML = template;
		node.querySelector("h2").innerHTML = response[i].name;
		node.querySelector(".level").innerHTML = response[i].level;
		node.querySelector(".exp").innerHTML = response[i].experience;		
		document.getElementById("lobbyAffichage").appendChild(node);
	}
}