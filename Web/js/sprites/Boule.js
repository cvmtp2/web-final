let positionFinDebutX;
let positionFinDebutY;

class Boule{
	constructor(couleur){
		this.speedX = 0;
		this.speedY = 0;
		this.velocity = -0.000001;
		this.couleur = couleur;
		/*
		if(couleur == "Jaune"){
			this.nodeJaune = document.getElementById("balleJaune");
		}else if(couleur == "Blanche"){
			this.nodeBlanche = document.getElementById("balleBlanche");
		}else if(couleur == "Rouge"){
			this.nodeRouge = document.getElementById("balleRouge");
		}*/

		this.borderTop = 30;
		this.borderBottom = document.getElementById("table").offsetHeight - 60;
		this.borderLeft = 30;
		this.borderRight = document.getElementById("table").offsetWidth - 60;

		if(couleur == "Jaune"){
			this.node = document.getElementById("balleJaune");
			this.xDebut = 0;
			this.yDebut = this.borderBottom / 2; 
			this.directionX  = "rien";
			this.directionY  = "rien"; 
			this.speedX = 0;
			this.speedY = 0;
		

		
		}
	}
	
	getBoundaries(){
		let temp = null;
	}
	move(){
		this.x -= this.speedX;
		this.y -= this.speedY;
	}
	drawImage(){
		this.node.style.left = this.x + "px";
		this.node.style.top = this.y + "px";
	}
	verifCollision(){
		let collision = false;

		return collision;
	}

	verifierTireBatton(x,y){
		
		
		//Tire

		//Droite haut
		if(x > 0 && y > 0){
			this.directionX  = "droite";
			this.directionY  = "haut";
		}
		//Droite bas
		else if(x > 0 && y < 0){
			this.directionX  = "droite";
			this.directionY  = "bas"
		}
		//gauche bas
		else if(x < 0 && y < 0){
			this.directionX = "gauche";
			this.directionY  = "bas"
		}
		//gauche haut
		else if(x < 0 && y > 0){
			this.directionX = "gauche";
			this.directionY  = "haut";
		}
		
		this.speedX = Math.abs(x);
		this.speedY = Math.abs(y);

	}
	
	verifCollision()
	{
		let collision = false;
		
		for (let i = 0; i < spriteListBoule.length; i++) {
			const element = spriteListBoule[i];
			if(element.couleur != this.couleur)
			{
				if(element.node.offsetLeft >= this.node.offsetLeft && element.node.offsetLeft <= this.node.offsetLeft + 7.5)
				{
					
				if(element.node.offsetTop >= this.node.offsetTop && element.node.offsetTop <= this.node.offsetTop + 30)
					{
						collision = true;
						

					}
				}
				else if(element.node.offsetLeft >= this.node.offsetLeft + 7.5 && element.node.offsetLeft <= this.node.offsetLeft + 15)
				{
					if(element.node.offsetTop >= this.node.offsetTop && element.node.offsetTop <= this.node.offsetTop + 30)
					{
						collision = true;
						if(element.node.style.top == this.node.style.top )
						{
							if(element.directionX == "rien")
							{
								
								element.directionX = "droite";
								this.directionX = "gauche";
								element.speedX = this.speedX;
							}
							else
							{
								element.directionX = "droite";
								this.directionX = "gauche";
								this.speedX = element.speedX;
							}
							
						}
						else //la balle vient d'en haut a droite
						{
							
							this.directionX = "gauche";
							element.directionX = "droite";
							element.directionY = this.directionY
							element.speedX = this.speedX;
							element.speedY = this.speedY;
						}

					}
					
					
				}
				else if(element.node.offsetLeft >= this.node.offsetLeft + 15 && element.node.offsetLeft <= this.node.offsetLeft + 22.5)
				{
					if(element.node.offsetTop >= this.node.offsetTop && element.node.offsetTop <= this.node.offsetTop + 30)
					{
						collision = true;

					}
				}
				else if(element.node.offsetLeft >= this.node.offsetLeft + 22.5 && element.node.offsetLeft <= this.node.offsetLeft + 30)
				{
					if(element.node.offsetTop >= this.node.offsetTop && element.node.offsetTop <= this.node.offsetTop + 30)
					{
						collision = true;
						if(element.node.style.top == this.node.style.top )
						{
							element.directionX = "droite";
							this.directionX = "gauche";
							this.speedX = element.speedX;
						}
						else //la balle vient d'en haut a droite
						{
							
							this.directionX = "gauche";
							element.directionX = "droite";
							element.directionY = this.directionY
							element.speedX = this.speedX;
							element.speedY = this.speedY;
						}

					}
				}
			}
		} 
		return collision;
	}

	calculeDirectionTire()
	{
		console.log("fds");
		positionFinDebutX = this.node.offsetTop;
		positionFinDebutY = this.node.offsetLeft;
		// La distance Y entre la position de la ball blanche et la position ou on clique le boutton gauche
		// si le distance que position, la ball va vers haut. Si la ditance que negatve, c'est l'inverse.
		
		//let distancePytagore = Math.sqrt(Math.pow((mouseQueueStartPosX - positionFinDebutX), 2)+Math.pow((mouseQueueStartPosY - positionFinDebutY), 2))
		let y = positionFinDebutY - mousePosY;
		let x =  positionFinDebutX - mousePosX;
		console.log(x);
		console.log(y);

		if(x > 0 && y > 0){
			this.directionX  = "droite";
			this.directionY  = "bas";
		}
		//Droite bas
		else if(x > 0 && y < 0){
			this.directionX  = "droite";
			this.directionY  = "haut"
		}
		//gauche bas
		else if(x < 0 && y < 0){
			this.directionX = "gauche";
			this.directionY  = "haut"
		}
		//gauche haut
		else if(x < 0 && y > 0){
			this.directionX = "gauche";
			this.directionY  = "bas";
		}
		else
		{
			this.directionX = "rien";
			this.directionY  = "rien";
		}
		
		this.speedX =2;
		this.speedY = 2;
	}
	


	tick(){
		
		let newY = this.node.offsetTop;
		let newX = this.node.offsetLeft;
	
		if(this.speedX != 0){
			this.speedX -= 0.01
		}
		if(this.speedY != 0){
			this.speedY -= 0.01
		}
		if(this.speedX < 0.1){
			this.speedX = 0
		}
		if(this.speedY < 0.1){
			this.speedY = 0
		}
		if(newY >= this.borderBottom )
		{
			this.directionY = "haut";
		} 
		else if(newY <= this.borderTop)
		{
			this.directionY  = "bas";
		}

		if(newX >= this.borderRight )
		{
			this.directionX = "gauche";
			
		} 
		else if(newX <= this.borderLeft)
		{
			this.directionX  = "droite";
		}


		if(this.directionY  == "bas" )
		{
			newY += this.speedY;
		}
		else
		{
			newY -= this.speedY;
		}

		if(this.directionX == "droite")
		{
			newX += this.speedX;
		}
		else
		{
			newX -= this.speedX;
		}
		//Lorsque les balles touchent le bord du gauche. Rebondir vers droite
		/*if (this.node.style.left < this.borderLeft+"px") {
			this.x += this.speedX;
		}

		//Lorsque les balles touchent le bord du droite. Rebondir vers gauche
		if (this.node.style.left > this.borderRight+"px") {
			this.x -= this.speedX;	
		}*/	

		this.node.style.top = newY + "px";
		this.node.style.left = newX+"px";

	}
}