let bouleX, bouleY;
class Queue{
	constructor(joueur){
		if(joueur == "1"){
			this.node = document.getElementById("queueJ1");
			this.boule = document.getElementById("balleBlanche");
		}else if(joueur == "2"){
			this.node = document.getElementById("queueJ2");
			this.boule = document.getElementById("balleJaune");
		}
		this.table = document.getElementById("table");
		this.x = 3 + "%";
		this.y = 10 + "%";
		this.force = 0;
		this.chargePlus = 0;

		//a efface apretour = true
		// tour = true
		
		document.getElementById("gameContainer").onmousemove = function (event) {

			mousePosX = event.pageX;
			mousePosY = event.pageY;
			

			if(tour == true){
				mousePosX = event.pageX;
				mousePosY = event.pageY;
				//console.log("Y: " + event.pageY);
			}
		}
		document.getElementById("gameContainer").onmousedown = function(event){
			charge = true;
			mouseQueueStartPosX = mousePosX;
			mouseQueueStartPosY = mousePosY;
		}
		document.getElementById("gameContainer").onmouseup = function(event){
			charge = false;
			let x = (bouleX - mouseQueueStartPosX)*0.05
			let y = (mouseQueueStartPosY -  bouleY)*0.05
			for (let i = 0; i < spriteListBoule.length; i++) 
			{	
				const element = spriteListBoule[i];
				
				if(element.couleur == "Blanche" && joueur == "1")
				{
					element.calculeDirectionTire();
				}
				else if(element.couleur == "Jaune" && joueur == "2")
				{
					element.calculeDirectionTire();
				}
			} 
			if(joueur == "1"){
				$.get('http://' + listening_IP + ':'+listening_port+'/api/addScore/'+idPartie+'/1', {}, function(data){
				});
			}
			else{
				$.get('http://' + listening_IP + ':'+listening_port+'/api/addScore/'+idPartie+'/2', {}, function(data){
				});
			}
			
			
		}
		/* if(tour == false){
			document.getElementById("passerTour").className = "disabled";
		}else{
			document.getElementById("passerTour").className = "";
		} */

	}
	
	rotationQueue(){
		
		//get coords boule
		bouleY = this.boule.offsetTop + this.table.offsetTop + (this.boule.offsetWidth/2);
		bouleX = this.boule.offsetLeft + this.table.offsetLeft+ (this.boule.offsetHeight/2);
		
		// let bouleY = this.boule.offsetTop + (this.boule.offsetWidth/2);
		// let bouleX = this.boule.offsetLeft + (this.boule.offsetHeight/2);
		let queueWidthCenter = this.node.offsetWidth/2;

		this.y = bouleY;
		this.x = bouleX - queueWidthCenter;

		//console.log(bouleY + " " + bouleX);
		this.node.style.top =  this.y + "px";
		this.node.style.left = this.x + "px";
		this.node.style.transform = "translate(100px,100px)";

		let angle = getElementAngle(mousePosX, mousePosY, bouleX,bouleY);
		rotateElement(this.node, angle);
	}
	chargeQueue(){
		if(charge == true){
			let base = 1.5; //pourcentage de base pour commencer a charger
			this.force = Math.sqrt( ((Math.abs(mousePosX - mouseQueueStartPosX))^2) + ((Math.abs(mousePosY - mouseQueueStartPosY))^2)); //pythagoras
			//console.log(this.force);

			this.node.style.paddingTop = (base+(this.force||0)) + "%";
		}else{
			if(this.force >= 0){
				//release power
				this.node.style.paddingTop = 1+"%";
				this.node.style.transition = "0.1s ease";
			}
		}
	}

	directionQueue(){
		console.log(bouleX);
		
	}

	tick(){
		let alive = true;

		if(tour == false){
			//console.log("not my turn");
			mousePosX = mousePosXAutre;
			mousePosY = mousePosYAutre;
		}else{
			//console.log("my turn");
		}
		
		this.rotationQueue();
		this.chargeQueue();
		// console.log(this.charge + " " + this.force);
		
		return alive;
	}
}