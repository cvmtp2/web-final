<?php	
	require_once('partial/header.php');	
	session_start();
	$_SESSION['session_idJoueur'] = $_GET['session_idJoueur'];
	$_SESSION['session_idPartie'] = $_GET['session_idPartie'];
	$_SESSION['session_nomJoueur'] = $_GET['session_nomJoueur'];
?>
<script src="js/js-lobby.js"></script>
<script>
idJoueur = <?php echo $_SESSION['session_idJoueur']; ?>;
idPartie = <?php echo $_SESSION['session_idPartie']; ?>;
nomJoueur = "<?php echo $_SESSION['session_nomJoueur']; ?>";
</script>
<div id="lobbyContainer">
	<div>
		<div class="gauche" id="lobbyButtons">
			<h3>Menu</h3>
			<button id="buttonParties">Parties</button>
			<div class="clear"></div>
			<button id="buttonScores">Scores</button>
			<div class="clear"></div>
			<button id="buttonInfos">Joueur</button>
		</div>
		<div class="clear"></div>		
		<div class="droite" id="lobbyAffichageScores">
			<div>Meilleurs Scores</div>
		</div>
		<div class="droite" id="lobbyAffichageInfos">
			<div>Data du joueur</div>
		</div>
		<div class="droite" id="lobbyAffichageParties">
			<div id="msg"></div>
			<div class="slotPartie" id="nouvellePartie">
				<h2 class="idPartie" id="new">+</h2>
				<div>
					<h4>Nouvelle partie</h4>
					<h5>Tu seras le joueur 1</h5>
				</div>
			</div>
			<div class="slotPartie" id="curentPartie">
				<h2 class="idPartie" id="1"><?php echo $_SESSION['session_idPartie']; ?></h2>
				<div>
					<h4>Joueurs dans la partie :</h4>
					<h5 id="playerName" style="color:#DC143C;font-weight: bold;"></h5>
					<h4>Type de partie</h4>
					<h5>Carom</h5>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="messageBox" class="question">
	<button id="back">Back</button>	
	<div>Type de partie :</div>
	<select id="typePartie">
		<option value="carom">Carom libre</option>
		<option value="une">Une Bande</option>
		<option value="trois">Trois bandes</option>
	</select>
	<button id="enter">Enter</button>
</div>

<?php
	require_once("partial/footer.php");