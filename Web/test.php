<?php 
	require_once("action/ClientAction.php");

	$action = new ClientAction();
	$action->execute();	
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Page Title</title>
	<link rel="stylesheet" href="css/global.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="js/jquery.min.js"></script>
	<script src="js/js-game.js"></script>
</head>
<body>
<!-- <div id="table"></div> -->
	<input type="text" id="nomJoueur">
	<button id="valider">idJoueur</button>
    <button id="debut">debut partie</button>
	<button id="finTour">fin tour</button>
    <div id="table"></div>
	<div id="test"></div>
</body>
</html>

